package no.uio.ifi.cflat.chargenerator;

/*
 * module CharGenerator
 */

import java.io.*;
import no.uio.ifi.cflat.cflat.Cflat;
import no.uio.ifi.cflat.error.Error;
import no.uio.ifi.cflat.log.Log;

/*
 * Module for reading single characters.
 */
public class CharGenerator {
    public static char curC, nextC;
	
    private static LineNumberReader sourceFile = null;
    private static String sourceLine;
    private static int sourcePos;
    //private static boolean timeToLog;
	
    public static void init() {
	try {
	    sourceFile = new LineNumberReader(new FileReader(Cflat.sourceName));
	} catch (FileNotFoundException e) {
	    Error.error("Cannot read " + Cflat.sourceName + "!");
	}
	sourceLine = "";  sourcePos = 0;  curC = nextC = ' ';
    //timeToLog = true;
    readNext();  readNext();

    }
	
    public static void finish() {
	if (sourceFile != null) {
	    try {
		sourceFile.close();
	    } catch (IOException e) {
		Error.error("Could not close source file!");
	    }
	}
    }

    public static boolean isMoreToRead() {
        return (sourceLine != null);
    }
	
    public static int curLineNum() {
	return (sourceFile == null ? 0 : sourceFile.getLineNumber());
    }

	
    public static void readNext() {
        curC = nextC;
        if (! isMoreToRead()) {
            nextC = ' ';
            return;
        }
        if (sourcePos < sourceLine.length() && sourceLine.charAt(0) != '#') {
            nextC = sourceLine.charAt(sourcePos ++);
        } else {
            try {
                sourceLine = sourceFile.readLine();

            } catch (IOException e) {
                Error.error("Could not read line from source file!");
            }
            if (isMoreToRead()) {
                Log.noteSourceLine(curLineNum(), sourceLine);
            }
            sourcePos = 0;
            readNext();
        }
    }
}

package no.uio.ifi.cflat.syntax;

/*
 * module Syntax
 */

import no.uio.ifi.cflat.cflat.Cflat;
import no.uio.ifi.cflat.code.Code;
import no.uio.ifi.cflat.error.Error;
import no.uio.ifi.cflat.log.Log;
import no.uio.ifi.cflat.scanner.Scanner;
import no.uio.ifi.cflat.scanner.Token;
import static no.uio.ifi.cflat.scanner.Token.*;
import no.uio.ifi.cflat.types.*;

/*
 * Creates a syntax tree by parsing; 
 * prints the parse tree (if requested);
 * checks it;
 * generates executable code. 
 */
public class Syntax {
    static DeclList library;
    static Program program;
    
    static void includeLib() {
        FuncDecl libFunc;
        libFunc = new FuncDecl(intToken, "exit", intToken, "status");
        library.addDecl(libFunc);
        libFunc = new FuncDecl(intToken, "getchar", null, "");
        library.addDecl(libFunc);
        libFunc = new FuncDecl(doubleToken, "getdouble", null, "");
        library.addDecl(libFunc);
        libFunc = new FuncDecl(intToken, "getint", null, "");
        library.addDecl(libFunc);
        libFunc = new FuncDecl(intToken, "putchar", intToken, "c");
        library.addDecl(libFunc);
        libFunc = new FuncDecl(doubleToken, "putdouble", doubleToken, "c");
        library.addDecl(libFunc);
        libFunc = new FuncDecl(intToken, "putint", intToken, "c");
        library.addDecl(libFunc);
    }

    public static void init() {
        library = new GlobalDeclList();
        includeLib();
    }

    public static void finish() {
    }

    public static void checkProgram() {
	program.check(library);
    }

    public static void genCode() {
	program.genCode(null);
    }

    public static void parseProgram() {
	program = new Program();
	program.parse();
    }

    public static void printProgram() {
	program.printTree();
    }

    static void error(SyntaxUnit use, String message) {
	Error.error(use.lineNum, message);
    }
}


/*
 * Master class for all syntactic units.
 * (This class is not mentioned in the syntax diagrams.)
 */
abstract class SyntaxUnit {
    int lineNum;

    SyntaxUnit() {
	lineNum = Scanner.curLine;
    }

    abstract void check(DeclList curDecls);
    abstract void genCode(FuncDecl curFunc);
    abstract void parse();
    abstract void printTree();
}


/*
 * A <program>
 */
class Program extends SyntaxUnit {
    DeclList progDecls = new GlobalDeclList();
	
    @Override void check(DeclList curDecls) {
	progDecls.check(curDecls);

	if (! Cflat.noLink) {
        FuncDecl mainFunc = (FuncDecl) progDecls.findDecl("main", this);
        if (mainFunc == null)
            Error.error("Name main is unknown!");
        else {
            if (mainFunc.type != Types.intType)
                Syntax.error(mainFunc, "Main should return int");
            if ( ((ParamDeclList)mainFunc.paramDecls).paramNum != 0)
                Syntax.error(mainFunc, "Main should take 0 parameters");
        }
	}
    }
		
    @Override void genCode(FuncDecl curFunc) {
	progDecls.genCode(null);
    }

    @Override void parse() {
	Log.enterParser("<program>");

	progDecls.parse();
	if (Scanner.curToken != eofToken)
	    Error.expected("A declaration");

	Log.leaveParser("</program>");
    }

    @Override void printTree() {
	progDecls.printTree();
    }
}


/*
 * A declaration list.
 * (This class is not mentioned in the syntax diagrams.)
 */

abstract class DeclList extends SyntaxUnit {
    Declaration firstDecl = null;
    DeclList outerScope;

    DeclList () {
    }

    @Override void check(DeclList curDecls) {
        outerScope = curDecls;

        Declaration dx = firstDecl;
        while (dx != null) {
            dx.check(this);  dx = dx.nextDecl;
        }
    }

    @Override void printTree() {
        Declaration dx = firstDecl;
	    while (dx != null) {
            dx.printTree();
            dx = dx.nextDecl;
        }
    }

    void addDecl(Declaration d) {
	    if (firstDecl == null) {
            firstDecl = d;
        } else {
            Declaration dx = firstDecl;
            while (dx.nextDecl != null) {
                if ((d.name).equals(dx.name)) Syntax.error(d,": '" + d.name + "' already declared");
                dx = dx.nextDecl;
            }
            if ((d.name).equals(dx.name)) Syntax.error(d,": '" + d.name + "' already declared");
            dx.nextDecl = d;
        }
    }

    int dataSize() {
        Declaration dx = firstDecl;
        int res = 0;

        while (dx != null) {
            res += dx.declSize();  dx = dx.nextDecl;
        }
        return res;
    }
    
    Declaration findDecl(String name, SyntaxUnit usedIn) {
	    Declaration dx = firstDecl;
        while (dx != null) {
            if ((dx.name).equals(name)) {
                if (outerScope == null) Log.noteBinding(name, usedIn.lineNum);
                else Log.noteBinding(name, dx.lineNum, usedIn.lineNum);

                return dx;
            }
            dx = dx.nextDecl;
        }
        if (outerScope != null) {
            return outerScope.findDecl(name, usedIn);
        }
	return null;
    }
}


/*
 * A list of global declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 */
class GlobalDeclList extends DeclList {
    @Override void genCode(FuncDecl curFunc) {
        Declaration d = firstDecl;
        while (d != null) {
            d.genCode(curFunc);
            d = d.nextDecl;
        }
    }

    @Override void parse() {
    Log.enterParser("<global decl list>");
	while (Token.isTypeName(Scanner.curToken)) {
	    if (Scanner.nextToken == nameToken) {
            if (Scanner.nextNextToken == leftParToken) {
                FuncDecl fd = new FuncDecl(Scanner.nextName);
                fd.parse();
                addDecl(fd);
            } else if (Scanner.nextNextToken == leftBracketToken) {
                GlobalArrayDecl gad = new GlobalArrayDecl(Scanner.nextName);
                gad.parse();
                addDecl(gad);
            } else {
                GlobalSimpleVarDecl gsvd = new GlobalSimpleVarDecl(Scanner.nextName);
                gsvd.parse();
                addDecl(gsvd);
            }
	    } else {
		Error.expected("A declaration");
	    }
	}
    Log.leaveParser("</global decl list>");
    }
}


/*
 * A list of local declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 */
class LocalDeclList extends DeclList {
    @Override void genCode(FuncDecl curFunc) {
        int offset = 0;
        Declaration d = firstDecl;
        while (d != null) {
            offset -= d.declSize();
            d.assemblerName = offset+"(%ebp)";
            d = d.nextDecl;
        }
        
        if (dataSize() > 0)
            Code.genInstr("", "subl", "$"+dataSize()+",%esp", "Get "+dataSize()+" bytes local data space");    
    }

    @Override void parse() {
        Log.enterParser("<local decl list>");
        while (Token.isTypeName(Scanner.curToken)) {
            if (Scanner.nextToken == nameToken) {
                if (Scanner.nextNextToken == leftBracketToken) {
                    LocalArrayDecl lad = new LocalArrayDecl(Scanner.nextName);
                    lad.parse();
                    addDecl(lad);
                } else {
                    LocalSimpleVarDecl lsvd = new LocalSimpleVarDecl(Scanner.nextName);
                    lsvd.parse();
                    addDecl(lsvd);
                }
            } else {
            Error.expected("A declaration");
            }
        }
        Log.leaveParser("</local decl list>");
    }
}


/*
 * A list of parameter declarations. 
 * (This class is not mentioned in the syntax diagrams.)
 */
class ParamDeclList extends DeclList {
    int paramNum = 0;
    
    @Override void genCode(FuncDecl curFunc) {
        int offset = 8; 
        Declaration d = firstDecl;
        while (d != null) {
            d.assemblerName = offset+"(%ebp)";
            offset += d.declSize();
            d = d.nextDecl;
        }
    }

    int declSize() {
        int size = 0;
        Declaration d = firstDecl;
        while (d != null) {
            size += d.declSize();
            d = d.nextDecl;
        }
        return size;
    }

    @Override void parse() {
        Log.enterParser("<param decl list>");
        while (Token.isTypeName(Scanner.curToken)) {
            if (Scanner.nextToken == nameToken) {
                ParamDecl pd = new ParamDecl(Scanner.nextName, paramNum++);
                pd.parse();
                addDecl(pd);
                if (Scanner.curToken == commaToken) Scanner.skip(commaToken);

            } else {
                Error.expected("A decleration");
            }
        }
        Log.leaveParser("</param decl list>");
    }
}


/*
 * Any kind of declaration.
 * (This class is not mentioned in the syntax diagrams.)
 */
abstract class Declaration extends SyntaxUnit {
    String name, assemblerName;
    Type type;
    boolean visible = false;
    Declaration nextDecl = null;

    Declaration(String n) {
	name = n;
    }

    abstract int declSize();

    /**
     * checkWhetherArray: Utility method to check whether this Declaration is
     * really an array. The compiler must check that a name is used properly;
     * for instance, using an array name a in "a()" or in "x=a;" is illegal.
     * This is handled in the following way:
     * <ul>
     * <li> When a name a is found in a setting which implies that should be an
     *      array (i.e., in a construct like "a["), the parser will first 
     *      search for a's declaration d.
     * <li> The parser will call d.checkWhetherArray(this).
     * <li> Every sub-class of Declaration will implement a checkWhetherArray.
     *      If the declaration is indeed an array, checkWhetherArray will do
     *      nothing, but if it is not, the method will give an error message.
     * </ul>
     * Examples
     * <dl>
     *  <dt>GlobalArrayDecl.checkWhetherArray(...)</dt>
     *  <dd>will do nothing, as everything is all right.</dd>
     *  <dt>FuncDecl.checkWhetherArray(...)</dt>
     *  <dd>will give an error message.</dd>
     * </dl>
     */
    abstract void checkWhetherArray(SyntaxUnit use);

    /**
     * checkWhetherFunction: Utility method to check whether this Declaration
     * is really a function.
     * 
     * @param nParamsUsed Number of parameters used in the actual call.
     *                    (The method will give an error message if the
     *                    function was used with too many or too few parameters.)
     * @param use From where is the check performed?
     * @see   checkWhetherArray
     */
    abstract void checkWhetherFunction(int nParamsUsed, SyntaxUnit use);

    /**
     * checkWhetherSimpleVar: Utility method to check whether this
     * Declaration is really a simple variable.
     *
     * @see   checkWhetherArray
     */
    abstract void checkWhetherSimpleVar(SyntaxUnit use);
}


/*
 * A <var decl>
 */
abstract class VarDecl extends Declaration {
    VarDecl(String n) {
	super(n);
    }

    void genStore(Type valType) {
        if (type == Types.doubleType && valType == Types.doubleType) {
            Code.genInstr("", "fstpl", assemblerName, name+" =");
        } else if (type == Types.intType && valType == Types.doubleType) {
            Code.genInstr("", "fistpl", assemblerName, name+" = (int)");
        } else if (type == Types.doubleType && valType == Types.intType) {
            Code.genInstr("", "movl", "%eax,"+Code.tmpLabel, "");
            Code.genInstr("", "fildl", Code.tmpLabel, "  (double)");
            Code.genInstr("", "fstpl", assemblerName, name+" =");
        } else if (type == Types.intType && valType == Types.intType) {
            Code.genInstr("", "movl", "%eax,"+assemblerName, name+" =");
        }
    }

    @Override int declSize() {
	return type.size();
    }

    @Override void checkWhetherFunction(int nParamsUsed, SyntaxUnit use) {
	Syntax.error(use, name + " is a variable and no function!");
    }
    
    @Override void checkWhetherArray(SyntaxUnit use) {
	Syntax.error(use, name + " is a variable and no array!");
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	/* OK */
    }
    
    @Override void parse() {
	Log.enterParser("<var decl>");
    type = Types.getType(Scanner.curToken);
    Scanner.readNext(); //consume type
    Scanner.skip(nameToken);
	Scanner.skip(semicolonToken);
	Log.leaveParser("</var decl>");
    }
	
    @Override void printTree() {
	Log.wTree(type.typeName() + " " + name);
	Log.wTreeLn(";");
    }
}


/*
 * A global array declaration
 */

abstract class ArrayDecl extends VarDecl {
    ArrayDecl(String n) {
	super(n);
    }

    void genStore(Type valType) {
        Code.genInstr("", "leal", assemblerName+",%edx", "");
        Code.genInstr("", "popl", "%ecx", "");
        if (((ArrayType)type).elemType == Types.doubleType && valType == Types.doubleType) {
            Code.genInstr("", "fstpl", "(%edx,%ecx,8)", name+"[...] =");
        } else if (((ArrayType)type).elemType == Types.intType && valType == Types.doubleType) {
            Code.genInstr("", "fistpl", "(%edx,%ecx,4)", name+"[...] = (int)");
        } else if (((ArrayType)type).elemType == Types.doubleType && valType == Types.intType) {
            Code.genInstr("", "movl", "%eax,"+Code.tmpLabel, "");
            Code.genInstr("", "fildl", Code.tmpLabel, "  (double)");
            Code.genInstr("", "fstpl", "(%edx,%ecx,8)", name+"[...] =");
        } else if (((ArrayType)type).elemType == Types.intType && valType == Types.intType) {
            Code.genInstr("", "movl", "%eax,(%edx,%ecx,4)", name+"[...] =");
        }
    }

    @Override void parse() {
	Log.enterParser("<array decl>");
    Type elemType = Types.getType(Scanner.curToken);
    Scanner.readNext(); //consume type
    Scanner.skip(nameToken);
    Scanner.skip(leftBracketToken);
    int nElems = Scanner.curNum;
    type = new ArrayType(nElems, elemType);
    Scanner.skip(numberToken);
    Scanner.skip(rightBracketToken);
    Scanner.skip(semicolonToken);

	Log.leaveParser("</array decl>");
    }

    @Override void printTree() {
        Log.wTreeLn(((ArrayType)type).elemType.typeName() + " " + name + "[" + ((ArrayType)type).nElems + "];");
    }
    
    @Override void checkWhetherArray(SyntaxUnit use) {
	/* OK */
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	Syntax.error(use, name + " is an array and no simple variable!");
    }
}

class GlobalArrayDecl extends ArrayDecl {
    GlobalArrayDecl(String n) {
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;
    }

    @Override void check(DeclList curDecls) {
        visible = true;
        if (((ArrayType)type).nElems < 0)
            Syntax.error(this, "Arrays cannot have negative size!");
    }


    @Override void genCode(FuncDecl curFunc) {
        Code.genVar(assemblerName, true, declSize(), ((ArrayType)type).elemType.typeName()+" "+name+"["+((ArrayType)type).nElems+"];");
    }
}


/*
 * A global simple variable declaration
 */
class GlobalSimpleVarDecl extends VarDecl {
    GlobalSimpleVarDecl(String n) {
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;
    }

    boolean isGlobal() { return true; }
    
    @Override void check(DeclList curDecls) {
        visible = true;
    }


    @Override void genCode(FuncDecl curFunc) {
        Code.genVar(assemblerName, true, declSize(), type.typeName()+" "+name+";");
    }

}


/*
 * A local array declaration
 */
class LocalArrayDecl extends ArrayDecl {
    LocalArrayDecl(String n) {
	super(n); 
    }

    @Override void check(DeclList curDecls) {
        visible = true;
        if (((ArrayType)type).nElems < 0)
            Syntax.error(this, "Arrays cannot have negative size!");
    }

    @Override void genCode(FuncDecl curFunc) {
    }

}


/*
 * A local simple variable declaration
 */
class LocalSimpleVarDecl extends VarDecl {
    LocalSimpleVarDecl(String n) {
	super(n); 
    }

    @Override void check(DeclList curDecls) {
        visible = true;
    }

    @Override void genCode(FuncDecl curFunc) {
    }
}


/*
 * A <param decl>
 */
class ParamDecl extends VarDecl {
    int paramNum = 0;

    ParamDecl(String n) {
	super(n);
    }

    ParamDecl(String n, int num) {
	    super(n);
        paramNum = num;
    }
    
    @Override void check(DeclList curDecls) {
        visible = true;
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	//-- Must be changed in part 2:
	Syntax.error(use, name + " is a variable and no array!");
    }

    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	//ok//
    }

    @Override void genCode(FuncDecl curFunc) {
    }

    @Override void parse() {
        Log.enterParser("<param decl>");
        type = Types.getType(Scanner.curToken);
        Scanner.readNext(); //consume type
        Scanner.skip(nameToken);
        Log.leaveParser("</param decl>");
    }

    @Override void printTree() {
        Log.wTree(type.typeName() + " " + name);
        if (nextDecl != null) Log.wTree(", ");
    }  
        
}


/*
 * A <func decl>
 */
class FuncDecl extends Declaration {
    DeclList paramDecls = new ParamDeclList();
    DeclList localDecls = new LocalDeclList();
    StatmList body = new StatmList();
	
    FuncDecl(String n) {
	super(n);
	assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + n;
    }

    //Høyst ugenerell metode for å klemme inn biblioteket i syntakstreet.
    FuncDecl(Token retType, String name, Token paramType, String paramName) {
        super(name);
	    assemblerName = (Cflat.underscoredGlobals() ? "_" : "") + name;
        type = Types.getType(retType);
        if (paramType != null) {
            ParamDecl param = new ParamDecl(paramName, 0);
            param.type = Types.getType(paramType);
            paramDecls.addDecl(param);
            ((ParamDeclList)paramDecls).paramNum = 1;
        }
        visible = true;
    }

    @Override int declSize() {
	return 0;
    }
    
    @Override void check(DeclList curDecls) {
        visible = true;
        paramDecls.check(curDecls);
        localDecls.check(paramDecls);
        body.setReturnType(type);
        body.check(localDecls);
    }

    @Override void checkWhetherArray(SyntaxUnit use) {
	    Syntax.error(use, name + " is a function and no array!");
    }

    @Override void checkWhetherFunction(int nParamsUsed, SyntaxUnit use) {
        if (((ParamDeclList)paramDecls).paramNum != nParamsUsed)
            Syntax.error(use, name + " called with wrong number of parameters");
    }
	
    @Override void checkWhetherSimpleVar(SyntaxUnit use) {
	    Syntax.error(use, name + " is a function and no simple variable!");
    }

    @Override void genCode(FuncDecl curFunc) {
	Code.genInstr("", ".globl", assemblerName, "");
	Code.genInstr(assemblerName, "pushl", "%ebp", "Start function "+name);
	Code.genInstr("", "movl", "%esp,%ebp", "");

    paramDecls.genCode(this);
    localDecls.genCode(this); //this?
    body.genCode(this);
    
    if (type == Types.doubleType) Code.genInstr("", "fldz", "", "");
    Code.genInstr(".exit$" + assemblerName, "", "", "");
    Code.genInstr("", "movl", "%ebp,%esp", "");
    Code.genInstr("", "popl", "%ebp", "");
    Code.genInstr("", "ret", "", "End function "+name);
    }

    @Override void parse() {
        Log.enterParser("<func decl");
        type = Types.getType(Scanner.curToken);
        Scanner.readNext(); //consume type
        Scanner.skip(nameToken);
        Scanner.skip(leftParToken);
        paramDecls.parse();
        Scanner.skip(rightParToken);
        Scanner.skip(leftCurlToken);
        localDecls.parse();
        body.parse();
        Scanner.skip(rightCurlToken);

        
        Log.leaveParser("</func decl");
    }

    @Override void printTree() {
	    Log.wTree(type.typeName() + " " + name + "("); paramDecls.printTree(); Log.wTreeLn(") {");
        Log.indentTree();
        localDecls.printTree();
        body.printTree();
        Log.outdentTree();
        Log.wTreeLn("}");
    
    }
}


/*
 * A <statm list>.
 */
class StatmList extends SyntaxUnit {
    Statement firstStatm = null;
    
    void addStatm(Statement s) {
        if (firstStatm == null) {
            firstStatm = s;
        } else {
            Statement sx = firstStatm;
            while (sx.nextStatm != null) {
                sx = sx.nextStatm;
            }
            sx.nextStatm = s;
        }
    }

    /*
     * Helper for setting a field in ReturnStatm's so they can be type checked.
     * Called from FuncDecl.check()
     */
    protected void setReturnType(Type type) {
        Statement sx = firstStatm;
        while (sx != null) {
            if (sx instanceof ReturnStatm) {
                ((ReturnStatm)sx).retType = type;
            } else if (sx instanceof ForStatm) {
                ((ForStatm)sx).body.setReturnType(type);
            } else if (sx instanceof WhileStatm) {
                ((WhileStatm)sx).body.setReturnType(type);
            } else if (sx instanceof IfStatm) {
                ((IfStatm)sx).body.setReturnType(type);
                if (((IfStatm)sx).elsePart != null) {
                    ((IfStatm)sx).elsePart.setReturnType(type);
                }
            }
            sx = sx.nextStatm;
        }
    }

    @Override void check(DeclList curDecls) {
	    Statement sx = firstStatm;
        while (sx != null) {
            sx.check(curDecls);
            sx = sx.nextStatm;
        }
    }

    @Override void genCode(FuncDecl curFunc) {
        Statement sx = firstStatm;
        while (sx != null) {
            sx.genCode(curFunc);
            sx = sx.nextStatm;
        }
    }

    @Override void parse() {
	Log.enterParser("<statm list>");

	Statement lastStatm = null;
	while (Scanner.curToken != rightCurlToken) {
	    Log.enterParser("<statement>");
	    lastStatm = Statement.makeNewStatement();
        lastStatm.parse();
        addStatm(lastStatm);
	    Log.leaveParser("</statement>");
	}

	Log.leaveParser("</statm list>");
    }

    @Override void printTree() {
        Statement sx = firstStatm;
	    while (sx != null) {
            sx.printTree();
            sx = sx.nextStatm;
        }
    }
}


/*
 * A <statement>.
 */
abstract class Statement extends SyntaxUnit {
    Statement nextStatm = null;

    static Statement makeNewStatement() {
	if (Scanner.curToken==nameToken && 
	    Scanner.nextToken==leftParToken) {
	    return new CallStatm();
	} else if (Scanner.curToken == nameToken) {
	    return new AssignStatm();
	} else if (Scanner.curToken == forToken) {
	    return new ForStatm();
    } else if (Scanner.curToken == ifToken) {
	    return new IfStatm();
	} else if (Scanner.curToken == returnToken) {
	    return new ReturnStatm();
	} else if (Scanner.curToken == whileToken) {
	    return new WhileStatm();
	} else if (Scanner.curToken == semicolonToken) {
	    return new EmptyStatm();
	} else {
	    Error.expected("A statement");
	}
	return null;  // Just to keep the Java compiler happy. :-)
    }
}




/*
 * An <empty statm>.
 */
class EmptyStatm extends Statement {

    @Override void check(DeclList curDecls) {
    }

    @Override void genCode(FuncDecl curFunc) {
    }

    @Override void parse() {
        Log.enterParser("<empty statm>");
	    Scanner.skip(semicolonToken);
        Log.leaveParser("</empty statm>");
    }

    @Override void printTree() {
	    Log.wTreeLn(";");
    }
}
	

/*
 * A <for-statm>.
 */
class ForStatm extends Statement {
    Assignment initial = new Assignment();
    Expression condition = new Expression();
    Assignment update = new Assignment();
    StatmList body = new StatmList();

    @Override void check(DeclList curDecls) {
        initial.check(curDecls);
        condition.check(curDecls);
        update.check(curDecls);
        body.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
        String testLabel = Code.getLocalLabel(),
               endLabel = Code.getLocalLabel();

        initial.genCode(curFunc);
        Code.genInstr(testLabel, "", "", "Start for-statement");
        condition.genCode(curFunc);
        condition.valType.genJumpIfZero(endLabel);
        body.genCode(curFunc);
        update.genCode(curFunc);
        Code.genInstr("", "jmp", testLabel, "");
        Code.genInstr(endLabel, "", "", "End for-statement");
    }

    @Override void parse() {
        Log.enterParser("<for statm>");
        Log.enterParser("<for control>");
        Scanner.skip(forToken);
        Scanner.skip(leftParToken);
        initial.parse();
        Scanner.skip(semicolonToken);
        condition.parse();
        Scanner.skip(semicolonToken);
        update.parse();
        Scanner.skip(rightParToken);
        Log.leaveParser("</for control>");
        Scanner.skip(leftCurlToken);
        body.parse();
        Scanner.skip(rightCurlToken);
        Log.leaveParser("</for statm>");
    }

    @Override void printTree() {
	    Log.wTree("for ("); initial.printTree(); Log.wTree("; "); condition.printTree();
                Log.wTree("; "); update.printTree(); Log.wTreeLn(") {");
        Log.indentTree();
        body.printTree();
        Log.outdentTree();
        Log.wTreeLn("}");

    }
}

/*
 * An <if-statm>.
 */
class IfStatm extends Statement {
    Expression test = new Expression();
    StatmList body = new StatmList();
    StatmList elsePart = null;

    @Override void check(DeclList curDecls) {
        test.check(curDecls);
        body.check(curDecls);
        if (elsePart != null) elsePart.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
        String endLabel = Code.getLocalLabel(),
               elseLabel = "";

        if (elsePart != null)
            elseLabel = Code.getLocalLabel();
    
        Code.genInstr("", "", "", "Start if-statement");
        test.genCode(curFunc);
        test.valType.genJumpIfZero( ((elsePart==null)?endLabel:elseLabel) );
        body.genCode(curFunc);
       
        if (elsePart != null) {
            Code.genInstr("", "jmp", endLabel, "");
            Code.genInstr(elseLabel, "", "", "  else-part");
            elsePart.genCode(curFunc);
        }
        Code.genInstr(endLabel, "", "", "End if-statement");
    }

    @Override void parse() {
        Log.enterParser("<if statm>");
        Scanner.skip(ifToken);
        Scanner.skip(leftParToken);
        test.parse();
        Scanner.skip(rightParToken);
        Scanner.skip(leftCurlToken);
        body.parse();
        Scanner.skip(rightCurlToken);
        if (Scanner.curToken == elseToken) {
            Log.enterParser("<else part>");
            Scanner.skip(elseToken);
            Scanner.skip(leftCurlToken);
            elsePart = new StatmList();
            elsePart.parse();
            Scanner.skip(rightCurlToken);
            Log.leaveParser("</else part>");
        }
        Log.leaveParser("</if statm>");
    }

    @Override void printTree() {
        Log.wTree("if ("); test.printTree(); Log.wTreeLn(") {");
        Log.indentTree();
        body.printTree();
        Log.outdentTree();
        if (elsePart == null) Log.wTreeLn("}");
        else {
            Log.wTreeLn("} else {");
            Log.indentTree();
            elsePart.printTree();
            Log.outdentTree();
            Log.wTreeLn("}");
        }
    }
}

class AssignStatm extends Statement {
    Assignment ass = new Assignment();

    @Override void printTree() {
        ass.printTree(); Log.wTreeLn(";");
    }

    @Override void parse() {
        Log.enterParser("<assign statm>");
        ass.parse();
        Scanner.skip(semicolonToken);
        Log.leaveParser("</assign statm>");
    }

    @Override void genCode(FuncDecl curFunc) {
        ass.genCode(curFunc);
    }
    
    @Override void check(DeclList curDecls) {
        ass.check(curDecls);
    }
}

/*
 * A <return-statm>.
 */
class ReturnStatm extends Statement {
    Expression retval = new Expression();
    Type retType;

    @Override void printTree() {
    Log.wTree("return "); retval.printTree(); Log.wTreeLn(";");
    }

    @Override void parse() {
        Log.enterParser("<return statm>");
        Scanner.skip(returnToken);
        retval.parse();
        Scanner.skip(semicolonToken);
        Log.leaveParser("</return statm>");
    }

    @Override void genCode(FuncDecl curFunc) {
        retval.genCode(curFunc);
        Code.genInstr("", "jmp", ".exit$"+curFunc.assemblerName, "Return-statement");
    }
    @Override void check(DeclList curDecls) {
        retval.check(curDecls);
        retval.valType.checkType(lineNum, retType, "Return value");
    }
}

class CallStatm extends Statement {
    FunctionCall func = new FunctionCall();

    @Override void printTree() {
        func.printTree(); Log.wTreeLn(";");
    }
    
    @Override void parse() {
        Log.enterParser("<call statm>");
        func.parse();
        Scanner.skip(semicolonToken);
        Log.leaveParser("</call statm>");
    }

    @Override void genCode(FuncDecl curFunc) {
        func.genCode(curFunc);
        if (func.valType == Types.doubleType) {
            Code.genInstr("", "fstps", Code.tmpLabel, "Remove return value.");
        }
    }

    @Override void check(DeclList curDecls) {
        func.check(curDecls);
    }
}

/*
 * A <while-statm>.
 */
class WhileStatm extends Statement {
    Expression test = new Expression();
    StatmList body = new StatmList();

    @Override void check(DeclList curDecls) {
	test.check(curDecls);
	body.check(curDecls);
    }

    @Override void genCode(FuncDecl curFunc) {
	String testLabel = Code.getLocalLabel(), 
	       endLabel  = Code.getLocalLabel();

	Code.genInstr(testLabel, "", "", "Start while-statement");
	test.genCode(curFunc);
	test.valType.genJumpIfZero(endLabel);
	body.genCode(curFunc);
	Code.genInstr("", "jmp", testLabel, "");
	Code.genInstr(endLabel, "", "", "End while-statement");
    }

    @Override void parse() {
	Log.enterParser("<while-statm>");

	Scanner.readNext();
	Scanner.skip(leftParToken);
	test.parse();
	Scanner.skip(rightParToken);
	Scanner.skip(leftCurlToken);
	body.parse();
	Scanner.skip(rightCurlToken);

	Log.leaveParser("</while-statm>");
    }

    @Override void printTree() {
	Log.wTree("while (");  test.printTree();  Log.wTreeLn(") {");
	Log.indentTree();
	body.printTree();
	Log.outdentTree();
	Log.wTreeLn("}");
    }
}

class Assignment extends SyntaxUnit {
    Variable var = new Variable();
    Expression exp = new Expression();

    @Override void printTree() {
        var.printTree(); Log.wTree(" = "); exp.printTree();
    }
    
    @Override void parse() {
        Log.enterParser("<assignment>");
        var.parse();
        Scanner.skip(assignToken);
        exp.parse();
        Log.leaveParser("</assignment>");
        
    }
    @Override void genCode(FuncDecl curFunc) {
        if (var.index != null) {
            var.index.genCode(curFunc);
            Code.genInstr("", "pushl", "%eax", "");
        }
        exp.genCode(curFunc);
        var.declRef.genStore(exp.valType);
    }
    @Override void check(DeclList curDecls) {
        var.check(curDecls);
        exp.check(curDecls);
    }
}
/*
 * An <expression list>.
 */

class ExprList extends SyntaxUnit {
    int paramNum = 0;
    Expression firstExpr = null;

    @Override void check(DeclList curDecls) {
        Expression e = firstExpr;
        while (e != null) {
            e.check(curDecls);
            e = e.nextExpr;
        }
    }

    @Override void genCode(FuncDecl curFunc) {
        //iterative reverse traversal
        Expression e;
        for (int p = paramNum; p >  0; p--) {
            e = firstExpr;
            for (int i = 1; i < p; i++) {
                e = e.nextExpr;
            }
            e.genCode(curFunc);

            if (e.valType == Types.intType) {
                Code.genInstr("", "pushl", "%eax", "Push parameter #"+p);
            } else {
                Code.genInstr("", "subl", "$"+e.valType.size()+",%esp", "");
                Code.genInstr("", "fstpl", "(%esp)", "Push parameter #"+p);
            }
        }
    }

    @Override void parse() {
        Expression lastExpr = null;

        Log.enterParser("<expr list>");
        while(Scanner.curToken != rightParToken) {
            if (lastExpr != null) Scanner.skip(commaToken);
            lastExpr = new Expression();
            lastExpr.parse();
            addExpr(lastExpr);
            paramNum++;
        }
        Log.leaveParser("</expr list>");
    }

    @Override void printTree() {
        Expression curExpr = firstExpr;
        while (curExpr != null) {
            curExpr.printTree();
            if (curExpr.nextExpr != null) Log.wTree(", ");
            curExpr = curExpr.nextExpr;
        }
    }

    void addExpr(Expression e) {
        if (firstExpr == null) {
            firstExpr = e;
        } else {
            Expression ex = firstExpr;
            while (ex.nextExpr != null) {
                ex = ex.nextExpr;
            }
            ex.nextExpr = e;
        }
    }
}


/*
 * An <expression>
 */
class Expression extends Operand {
    Expression nextExpr = null;
    Term firstTerm = new Term(), secondTerm = null;
    Operator relOp = null;
    boolean innerExpr = false;

    @Override void check(DeclList curDecls) {
        firstTerm.check(curDecls);
        valType = firstTerm.valType;
        if (relOp != null) {
            relOp.check(curDecls);
            relOp.opType = valType;
    	    secondTerm.check(curDecls);
            valType.checkSameType(lineNum, secondTerm.valType, "Comparison operands");
            valType = Types.intType;
        }
    }

    @Override void genCode(FuncDecl curFunc) {
        firstTerm.genCode(curFunc);
        if (relOp != null) {
            if (firstTerm.valType == Types.intType) {
                Code.genInstr("", "pushl", "%eax", "");
            } else {
                Code.genInstr("", "subl", "$8,%esp", "");
                Code.genInstr("", "fstpl", "(%esp)", "");
            }
            secondTerm.genCode(curFunc);
            relOp.genCode(curFunc);
        }
    }

    @Override void parse() {
	Log.enterParser("<expression>");

	firstTerm.parse();
	if (Token.isRelOperator(Scanner.curToken)) {
	    relOp = new RelOperator(); 
	    relOp.parse();
	    secondTerm = new Term();
	    secondTerm.parse();
	}

	Log.leaveParser("</expression>");
    }

    @Override void printTree() {
        if (innerExpr) Log.wTree("(");
        firstTerm.printTree();
        if (relOp != null) {
            relOp.printTree();
            secondTerm.printTree();
        }
        if (innerExpr) Log.wTree(")");
    }
}


/*
 * A <term>
 */
class Term extends SyntaxUnit {
    Term nextTerm = null;
    Factor firstFac = null;
    TermOperator firstOp = null;   
    Type valType; 
    
    void addFactor(Factor f) {
        if (firstFac == null) firstFac = f;
        else {
            Factor fx = firstFac;
            while (fx.nextFactor != null) fx = fx.nextFactor;
            fx.nextFactor = f;
        }
    }

    void addTermOp(TermOperator o) {
        if (firstOp == null) firstOp = o;
        else {
            TermOperator ox = firstOp;
            while (ox.nextOp != null) ox = (TermOperator)ox.nextOp;
            ox.nextOp = o;
        }
    }

    @Override void check(DeclList curDecls) {
        Factor f = firstFac;
        TermOperator o = firstOp;
        f.check(curDecls);
        valType = f.valType;
        while (o != null) {
            o.check(curDecls);
            o.opType = f.valType;
            f = f.nextFactor;
            f.check(curDecls);
            o.opType.checkSameType(lineNum, f.valType, "Operands");
            o = (TermOperator)o.nextOp;
        }
    }

    @Override void genCode(FuncDecl curFunc) {
        Operator o = firstOp;
        Factor f = firstFac;
        f.genCode(curFunc);
        while (o != null) {
            if (valType == Types.intType) {
                Code.genInstr("", "pushl", "%eax", "");
            } else {
                Code.genInstr("", "subl", "$8,%esp", "");
                Code.genInstr("", "fstpl", "(%esp)", "");
            }
            f = f.nextFactor;
            f.genCode(curFunc);
            o.genCode(curFunc);
            o = o.nextOp;
        }
    }

    @Override void parse() {
        Factor fact;
        TermOperator termOp;
        Log.enterParser("<term>");
        while (Token.isOperand(Scanner.curToken)) {
            fact = new Factor();
            fact.parse();
            addFactor(fact);
            if (Token.isOperand(Scanner.curToken)) {
                Error.expected("an Operand");
            } 
            if (Token.isTermOperator(Scanner.curToken)) {
                termOp = new TermOperator();
                termOp.parse();
                addTermOp(termOp);
            }
        }
        Log.leaveParser("</term>");

    }

    @Override void printTree() {
        Factor fac = firstFac;
        TermOperator top = firstOp;
        while (fac != null) {
            fac.printTree();
            fac = fac.nextFactor;
            if (top != null) {
                top.printTree();
                top = (TermOperator)top.nextOp;
            }
        }
    }
}

class Factor extends SyntaxUnit {
    Factor nextFactor = null;
    Operand firstOperand = null;
    FacOperator firstFacOp = null;
    Type valType;

    void addOperand(Operand o) {
        if (firstOperand == null) firstOperand = o;
        else {
            Operand ox = firstOperand;
            while (ox.nextOperand != null) ox = ox.nextOperand;
            ox.nextOperand = o;
        }
    }

    void addFacOperator(FacOperator fop) {
        if (firstFacOp == null) firstFacOp = fop;
        else {
            FacOperator fox = firstFacOp;
            while (fox.nextOp != null) fox = (FacOperator)fox.nextOp;
            fox.nextOp = fop;
        }
    }

    @Override void check(DeclList curDecls) {
        Operand o = firstOperand;
        FacOperator f = firstFacOp;
        o.check(curDecls);
        valType = o.valType;
        while (f != null) {
            f.check(curDecls);
            f.opType = o.valType;
            o = o.nextOperand;
            o.check(curDecls);
            f.opType.checkSameType(lineNum, o.valType, "Operands");
            f = (FacOperator)f.nextOp;
        }
    }

    @Override void genCode(FuncDecl curFunc) {
        Operand o = firstOperand;
        Operator f = firstFacOp;
        o.genCode(curFunc);
        while (f != null) {
            if (valType == Types.intType) {
                Code.genInstr("", "pushl", "%eax", "");
            } else {
                Code.genInstr("", "subl", "$8,%esp", "");
                Code.genInstr("", "fstpl", "(%esp)", "");
            }
            o = o.nextOperand;
            o.genCode(curFunc);
            f.genCode(curFunc);
            f = f.nextOp;
        }
    }
    
    @Override void parse() {
        Operand op;
        FacOperator fop;
        Log.enterParser("<factor>");
        while (Token.isOperand(Scanner.curToken)) {
            Log.enterParser("<operand>");
            if ((Scanner.curToken == nameToken) && (Scanner.nextToken == leftParToken)) {
                op = new FunctionCall();
                op.parse();
                addOperand(op);
            } else if (Scanner.curToken == nameToken) {
                op = new Variable();
                op.parse();
                addOperand(op);
            } else if (Scanner.curToken == numberToken) {
                op = new Number();
                op.parse();
                addOperand(op);
            } else if (Scanner.curToken == leftParToken) {
                Scanner.skip(leftParToken);
                op = new Expression();
                ((Expression)op).innerExpr = true;
                op.parse();
                addOperand(op);
                Scanner.skip(rightParToken);
            } else {
                Error.expected("a Operand");
            }
            Log.leaveParser("</operand>");
            
            if (Token.isOperand(Scanner.curToken)) {
                Error.expected("an Operand");
            } 
            if (Token.isFactorOperator(Scanner.curToken)) {
                fop = new FacOperator();
                fop.parse();
                addFacOperator(fop);
            }
        }
        Log.leaveParser("</factor>");
    }

    @Override void printTree() {
        Operand op = firstOperand;
        FacOperator fop = firstFacOp;
        while (op != null) {
            op.printTree();
            op = op.nextOperand;
            if (fop != null) {
                fop.printTree();
                fop = (FacOperator)fop.nextOp;
            }
        }
    }
}

/*
 * An <operator>
 */
abstract class Operator extends SyntaxUnit {
    Operator nextOp = null;
    Type opType;
    Token opToken;

    @Override void check(DeclList curDecls) {}
}


class FacOperator extends Operator {
    @Override void genCode(FuncDecl curFunc) {
        if (opType == Types.intType) {
            Code.genInstr("", "movl", "%eax,%ecx", "");
            Code.genInstr("", "popl", "%eax"     , "");
            switch (opToken) {
                case multiplyToken:
                    Code.genInstr("", "imull", "%ecx,%eax", "Compute *"); break;
                case divideToken:
                    Code.genInstr("", "cdq", "", ""); 
                    Code.genInstr("", "idivl", "%ecx", "Compute /"); 
                    break;
            }
        } else {
            Code.genInstr("", "fldl", "(%esp)", "");
            Code.genInstr("", "addl", "$8,%esp", "");
            switch (opToken) {
                case multiplyToken:
                    Code.genInstr("", "fmulp", "", "Compute *"); break;
                case divideToken:
                    Code.genInstr("", "fdivp", "", "Compute /"); break;
            }

        }
    }

    @Override void parse() {
        Log.enterParser("<fac operator>");

        opToken = Scanner.curToken;
        Scanner.readNext();

        Log.leaveParser("</fac operator>");
    }

    @Override void printTree() {
        String op = "?";
        switch (opToken) {
        case divideToken:        op = "/";  break;
        case multiplyToken:      op = "*";  break;
        }
        Log.wTree("" + op + "");
    }

}

class TermOperator extends Operator {
    @Override void genCode(FuncDecl curFunc) {
        if (opType == Types.intType) {
            Code.genInstr("", "movl", "%eax,%ecx", "");
            Code.genInstr("", "popl", "%eax"     , "");
            switch (opToken) {
                case addToken:
                    Code.genInstr("", "addl", "%ecx,%eax", "Compute +"); break;
                case subtractToken:
                    Code.genInstr("", "subl", "%ecx,%eax", "Compute -"); break;
            }
        } else {
            Code.genInstr("", "fldl", "(%esp)", "");
            Code.genInstr("", "addl", "$8,%esp", "");
            switch (opToken) {
                case addToken:
                    Code.genInstr("", "faddp", "", "Compute +"); break;
                case subtractToken:
                    Code.genInstr("", "fsubp", "", "Compute -"); break;
            }

        }
    }
    
    @Override void parse() {
        Log.enterParser("<term operator>");

        opToken = Scanner.curToken;
        Scanner.readNext();

        Log.leaveParser("</term operator>");
    }
    
    @Override void printTree() {
        String op = "?";
        switch (opToken) {
        case subtractToken:        op = "-";  break;
        case addToken:         op = "+";  break;
        }
        Log.wTree(" " + op + " ");
    }
}

/*
 * A relational operator (==, !=, <, <=, > or >=).
 */

class RelOperator extends Operator {
    @Override void genCode(FuncDecl curFunc) {
	if (opType == Types.doubleType) {
	    Code.genInstr("", "fldl", "(%esp)", "");
	    Code.genInstr("", "addl", "$8,%esp", "");
	    Code.genInstr("", "fsubp", "", "");
	    Code.genInstr("", "fstps", Code.tmpLabel, "");
	    Code.genInstr("", "cmpl", "$0,"+Code.tmpLabel, "");
	} else {
	    Code.genInstr("", "popl", "%ecx", "");
	    Code.genInstr("", "cmpl", "%eax,%ecx", "");
	}
	Code.genInstr("", "movl", "$0,%eax", "");
	switch (opToken) {
	case equalToken:        
	    Code.genInstr("", "sete", "%al", "Test ==");  break;
	case notEqualToken:
	    Code.genInstr("", "setne", "%al", "Test !=");  break;
	case lessToken:
	    Code.genInstr("", "setl", "%al", "Test <");  break;
	case lessEqualToken:
	    Code.genInstr("", "setle", "%al", "Test <=");  break;
	case greaterToken:
	    Code.genInstr("", "setg", "%al", "Test >");  break;
	case greaterEqualToken:
	    Code.genInstr("", "setge", "%al", "Test >=");  break;
	}
    }

    @Override void parse() {
	Log.enterParser("<rel operator>");

	opToken = Scanner.curToken;
	Scanner.readNext();

	Log.leaveParser("</rel operator>");
    }

    @Override void printTree() {
	String op = "?";
	switch (opToken) {
	case equalToken:        op = "==";  break;
	case notEqualToken:     op = "!=";  break;
	case lessToken:         op = "<";   break;
	case lessEqualToken:    op = "<=";  break;
	case greaterToken:      op = ">";   break;
	case greaterEqualToken: op = ">=";  break;
	}
	Log.wTree(" " + op + " ");
    }
}


/*
 * An <operand>
 */
abstract class Operand extends SyntaxUnit {
    Operand nextOperand = null;
    Type valType;
}


/*
 * A <function call>.
 */
class FunctionCall extends Operand {
    String funcName;
    FuncDecl declRef = null;
    ExprList args = new ExprList();

    @Override void check(DeclList curDecls) {
	    Declaration d = curDecls.findDecl(funcName,this);
        if (d == null || !d.visible) Syntax.error(this, "" + d.name + " is unknown");
        args.check(curDecls);
        d.checkWhetherFunction(args.paramNum, this);
        valType = d.type;
        declRef = (FuncDecl)d;

        Declaration param = declRef.paramDecls.firstDecl;
        Expression expr = args.firstExpr;
        while (param != null) {
            expr.valType.checkType(lineNum, param.type, "Parameter #" + (((ParamDecl)param).paramNum+1));
            expr = expr.nextExpr;
            param = param.nextDecl;
        }
    }

    @Override void genCode(FuncDecl curFunc) {
        int paramNum = ((ParamDeclList)declRef.paramDecls).paramNum;
        int declSize =  ((ParamDeclList)declRef.paramDecls).declSize();

        args.genCode(curFunc);
        Code.genInstr("", "call", declRef.assemblerName, "Call "+funcName);
        if (paramNum > 0) {
            Code.genInstr("", "addl", "$"+declSize+",%esp", "Remove parameters");
        }
    }

    @Override void parse() {
        Log.enterParser("<function call>");
        funcName = Scanner.curName;
        Scanner.skip(nameToken);
        Scanner.skip(leftParToken);
        if (Scanner.curToken != rightParToken) {
            args.parse();
        }
        Scanner.skip(rightParToken);
        Log.leaveParser("</function call>");
    }

    @Override void printTree() {
        Log.wTree(funcName + "("); 
        args.printTree();
        Log.wTree(")");
    }
}


/*
 * A <number>.
 */
class Number extends Operand {
    int numVal;

    @Override void check(DeclList curDecls) {
        valType = Types.intType;
    }
	
    @Override void genCode(FuncDecl curFunc) {
	Code.genInstr("", "movl", "$"+numVal+",%eax", ""+numVal); 
    }

    @Override void parse() {
        Log.enterParser("<number>");
        numVal = Scanner.curNum;
        Scanner.skip(numberToken);
        Log.leaveParser("</number>");
    }

    @Override void printTree() {
	Log.wTree("" + numVal);
    }
}


/*
 * A <variable>.
 */

class Variable extends Operand {
    String varName;
    VarDecl declRef = null;
    Expression index = null;

    @Override void check(DeclList curDecls) {
	Declaration d = curDecls.findDecl(varName,this);
    if (d == null || !d.visible) Syntax.error(this, "" + d.name + " is unknown");
	if (index == null) {
	    d.checkWhetherSimpleVar(this);
	    valType = d.type;
	} else {
	    d.checkWhetherArray(this);
	    index.check(curDecls);
	    index.valType.checkType(lineNum, Types.intType, "Array index");
	    valType = ((ArrayType)d.type).elemType;
	}
	declRef = (VarDecl)d;
    }

    @Override void genCode(FuncDecl curFunc) {
        if (index == null) {
            if (valType == Types.intType) {
                Code.genInstr("", "movl", declRef.assemblerName+",%eax", varName);
            } else {
                Code.genInstr("", "fldl", declRef.assemblerName, varName);
            }
        } else {
            index.genCode(curFunc);
            Code.genInstr("", "leal", declRef.assemblerName+",%edx", varName+"[...]");
            if (valType == Types.intType) {
                Code.genInstr("", "movl", "(%edx,%eax,"+valType.size()+"),%eax", "");
            } else {
                Code.genInstr("", "fldl", "(%edx,%eax,"+valType.size()+")", "");
            }
        }
    }

    @Override void parse() {
	Log.enterParser("<variable>");
    varName = Scanner.curName;
    Scanner.skip(nameToken);
    if (Scanner.curToken == leftBracketToken) {
        Scanner.skip(leftBracketToken);
        index = new Expression();
        index.parse();
        Scanner.skip(rightBracketToken);
    }
	Log.leaveParser("</variable>");
    }

    @Override void printTree() {
        Log.wTree(varName);
        if (index != null) {
            Log.wTree("["); index.printTree(); Log.wTree("]");
        }
    }
}

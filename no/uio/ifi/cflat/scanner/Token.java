package no.uio.ifi.cflat.scanner;

/*
 * class Token
 */

/*
 * The different kinds of tokens read by Scanner.
 */
public enum Token { 
    addToken, assignToken, 
    commaToken, 
    divideToken, doubleToken,
    elseToken, eofToken, equalToken, 
    forToken, 
    greaterEqualToken, greaterToken, 
    ifToken, intToken, 
    leftBracketToken, leftCurlToken, leftParToken, lessEqualToken, lessToken, 
    multiplyToken, 
    nameToken, notEqualToken, numberToken, 
    rightBracketToken, rightCurlToken, rightParToken, returnToken, 
    semicolonToken, subtractToken, 
    whileToken;

    public static boolean isFactorOperator(Token t) {
	    switch (t) {
            case divideToken: break;
            case multiplyToken: break;
            default: return false;
        }
        return true;
    }

    public static boolean isTermOperator(Token t) {
	    switch (t) {
            case addToken: break;
            case subtractToken: break;
            default: return false;
        }
        return true;
    }

    public static boolean isRelOperator(Token t) {
        switch (t) {
            case equalToken: break;
            case greaterEqualToken: break;
            case greaterToken: break;
            case lessEqualToken: break;
            case lessToken: break;
            case notEqualToken: break;
            default: return false;
        }
        return true;
    }

    public static boolean isOperand(Token t) {
	    switch (t) {
            case numberToken: break;
            case nameToken: break;
            case leftParToken: break; //for å kunne bruke denne til test for "inner expression"
            default: return false;
        }
        return true;
    }

    public static boolean isTypeName(Token t) {
	    switch (t) {
            case doubleToken: break;
            case intToken: break;
            default: return false;
        }
        return true;
    }
}

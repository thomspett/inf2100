package no.uio.ifi.cflat.scanner;

/*
 * module Scanner
 */

import no.uio.ifi.cflat.chargenerator.CharGenerator;
import no.uio.ifi.cflat.error.Error;
import no.uio.ifi.cflat.log.Log;
import static no.uio.ifi.cflat.scanner.Token.*;

/*
 * Module for forming characters into tokens.
 */
public class Scanner {
    public static Token curToken, nextToken, nextNextToken;
    public static String curName, nextName, nextNextName;
    public static int curNum, nextNum, nextNextNum;
    public static int curLine, nextLine, nextNextLine;
    private static boolean gotLastOne;
	
    public static void init() {
	//-- Must be changed in part 0:
    nextNextLine = 0;
    gotLastOne = false;
    readNext();
    readNext();
    readNext();
    }
	
    public static void finish() {
	//-- Must be changed in part 0:
    }

    private static boolean isMoreToRead() {
        if (!CharGenerator.isMoreToRead() && gotLastOne) {
            return false;
        } else if (!CharGenerator.isMoreToRead()) {
            gotLastOne = true;
        }
        return true;
    }
	
    public static void readNext() {
    StringBuffer sb = new StringBuffer();
	curToken = nextToken;  nextToken = nextNextToken;
	curName = nextName;  nextName = nextNextName;
	curNum = nextNum;  nextNum = nextNextNum;
	curLine = nextLine;  nextLine = nextNextLine;
	
    nextNextToken = null;
	while (nextNextToken == null) {
        skipComments();
	    nextNextLine = CharGenerator.curLineNum();

	    if (!isMoreToRead()) {
		    nextNextToken = eofToken;
	    } else {
            if (isLegalChar(CharGenerator.curC)) {
                sb.append(CharGenerator.curC);
                nextNextToken = matchToken(sb.toString());
            } else {
                Error.error(nextNextLine,
                        "Illegal symbol: '" + CharGenerator.curC + "'!");
            }
	    }
        CharGenerator.readNext();
    }
	Log.noteToken();
    }

    private static Token matchToken(String s) {
        Token t = null;
        char nextC = CharGenerator.nextC;
        
        s = s.replaceAll("^\\s+","");
        if (s.length() == 0) return null;
        
        //Tokenize relational operators and assignment
        if (nextC != '=') {
            if (s.equals(">")) t = greaterToken;
            else if (s.equals("<")) t = lessToken;
            else if (s.equals("=")) t = assignToken;
        }

        if (s.equals(">=")) t = greaterEqualToken;
        else if (s.equals("<=")) t = lessEqualToken;
        else if (s.equals("==")) t = equalToken;
        else if (s.equals("!=")) t = notEqualToken;
        
        //tokenize single-char tokens
        else if (s.equals("(")) t = leftParToken;
        else if (s.equals(")")) t = rightParToken;
        else if (s.equals("[")) t = leftBracketToken;
        else if (s.equals("]")) t = rightBracketToken;
        else if (s.equals("{")) t = leftCurlToken;
        else if (s.equals("}")) t = rightCurlToken;
        else if (s.equals(";")) t = semicolonToken;
        else if (s.equals(",")) t = commaToken;
        else if (s.equals("+")) t = addToken;
        else if (s.equals("*")) t = multiplyToken;
        else if (s.equals("/")) t = divideToken;
        
        if (s.equals("-") &&
                (isWhiteSpace(nextC) || isLetterAZ(nextC) || (nextC == '(')))  {
            t = subtractToken;
        }

        //tokenize number
        if (t == null) {
        if (!isDigit(nextC) && (s.charAt(0) == '-' || isDigit(s.charAt(0)))) {
            t = numberToken;
            nextNextNum = Integer.parseInt(s);
        }
        if (s.length() == 2 && s.charAt(0) == '\'' && nextC == '\'') {
            t = numberToken;
            nextNextNum = s.charAt(1);
            CharGenerator.readNext();
        }
        }

        //tokenize words
        if (t == null) {
            if (isWhiteSpace(nextC) || isSpecialChar(nextC)) {
                if (s.equals("int")) { 
                    t = intToken;
                } else if (s.equals("double")) {
                    t = doubleToken;
                } else if (s.equals("return")) {
                    t = returnToken;
                } else if (s.equals("for")) {
                    t = forToken;
                } else if (s.equals("while")) {
                    t = whileToken;
                } else if (s.equals("if")) {
                    t = ifToken;
                } else if (s.equals("else")) {
                    t = elseToken;
                } else if (isLegalName(s)) {
                    t = nameToken;    
                    nextNextName = s;
                }
            }
        }
        return t;
    }

    private static void skipComments() {
        if (CharGenerator.curC == '/' && CharGenerator.nextC == '*') {
            CharGenerator.readNext();
            CharGenerator.readNext();
            while (! (CharGenerator.curC == '*' && CharGenerator.nextC == '/') ) {
                CharGenerator.readNext();
                if (! CharGenerator.isMoreToRead()) break;
            }
            CharGenerator.readNext();
            CharGenerator.readNext();
            skipComments(); //there may still be comments hiding out there!
        }
    }
    
    private static boolean isLegalName(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (! isLegalNameChar(s.charAt(i))) return false;
        }
        return true;
    }

    private static boolean isWhiteSpace(char c) {
        if (c == ' ') return true;
        if (c == '\t') return true;
        return false;
    }

    private static boolean isLegalChar(char c) {
        return c >= 0 && c <= 255;
    }

    private static boolean isLegalNameChar(char c) {
        if (c == '_') return true;
        if (isLetterAZ(c)) return true;
        if (isDigit(c)) return true;
        return false;
    }

    private static boolean isSpecialChar(char c) {
        if (c == '!') return true;
        if (c >= '\'' && c <= '-') return true;
        if (c == '/') return true;
        if (c >= ';' && c <= '>') return true;
        if (c == '[') return true;
        if (c == ']') return true;
        if (c == '{') return true;
        if (c == '}') return true;
        return false;
    }
       
    private static boolean isDigit(char c) {
        if (c >= '0' && c <= '9') return true;
        return false;
    }

    private static boolean isLetterAZ(char c) {
	    if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) return true;
        return false;
    }
	
    public static void check(Token t) {
	if (curToken != t)
	    Error.expected("A " + t);
    }
	
    public static void check(Token t1, Token t2) {
	if (curToken != t1 && curToken != t2)
	    Error.expected("A " + t1 + " or a " + t2);
    }
	
    public static void skip(Token t) {
	check(t);  readNext();
    }
	
    public static void skip(Token t1, Token t2) {
	check(t1,t2);  readNext();
    }
}

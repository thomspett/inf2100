#! /bin/bash

total=0
pass=0
fail=0

for file in `ls *.cflat`
do
    let "total += 1" 
    sourcefile=$file
    basename=${sourcefile%.*}
    java -jar ../../Cless.jar $sourcefile >/dev/null
    mv $basename.s $basename.ref
    java -jar ../../Cflat.jar $sourcefile >/dev/null
    result=$(diff -b $basename.s $basename.ref)
    if [ -n "$result" ]
    then
        echo ""
        echo $sourcefile
        echo ""
        diff -b $basename.s $basename.ref
        let "fail += 1"
    else
        let "pass += 1"
    fi
done

echo ""
echo ""
echo ""
echo "total: $total   pass: $pass   fail: $fail"
echo ""

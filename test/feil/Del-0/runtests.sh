#! /bin/bash
status=0
for file in `ls *.cflat`; 
do 
    echo "java -jar ../../../Cflat.jar $file"; 
    java -jar ../../../Cflat.jar $file; 
    let "status += $?"
done

exit $status

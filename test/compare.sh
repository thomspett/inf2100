#! /bin/bash

sourcefile=$1
basename=${sourcefile%.*}
java -jar ../Cless.jar -c $sourcefile >/dev/null
mv $basename.s $basename.ref
java -jar ../Cflat.jar -c $sourcefile >/dev/null
diff -b $basename.s $basename.ref
